class Happy
{
    int n;
    Happy()
    {  n=0; }
    void getnum(int nm)
    {
        n = nm;
    }
    int sum_sq_digits(int x)
    {
        if(x==0)
        {   return 0;   }
        return ((int)Math.pow(x%10,2))+sum_sq_digits(x/10);
    }
    void ishappy()
    {
        int q = n;
        while(true)
        {
            int sum = sum_sq_digits(q);
            if(q==1)
            {   System.out.println(n+" is a happy number");
                return;
            }
            else if(q<10)
            {   System.out.println(n+" is not a happy number");
                return;
            }
            q = sum;
        }
    }
    public static void main()
    {
        Happy ob = new Happy();
        ob.getnum(100);
        ob.ishappy();
        ob.getnum(28);
        ob.ishappy();
        ob.getnum(68);
        ob.ishappy();
        ob.getnum(12);
        ob.ishappy();
    }
}